EESchema Schematic File Version 2
LIBS:ftuart2-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ftdi
LIBS:74xgxx
LIBS:ac-dc
LIBS:actel
LIBS:Altera
LIBS:analog_devices
LIBS:battery_management
LIBS:bbd
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:diode
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:ir
LIBS:Lattice
LIBS:logo
LIBS:maxim
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic32mcu
LIBS:motor_drivers
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:Oscillators
LIBS:Power_Management
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:rfcom
LIBS:sensors
LIBS:silabs
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:Worldsemi
LIBS:Xicor
LIBS:Zilog
LIBS:ftuart2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ftuart2"
Date "2018-01-20"
Rev "V1"
Comp ""
Comment1 "DURE usb to uart"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FT2232H U4
U 1 1 5A62B246
P 6050 3775
F 0 "U4" H 5000 5875 50  0000 L CNN
F 1 "FT2232H" H 6800 5875 50  0000 L CNN
F 2 "Housings_QFP:TQFP-64_10x10mm_Pitch0.5mm" H 6050 3775 50  0001 C CNN
F 3 "" H 6050 3775 50  0001 C CNN
	1    6050 3775
	1    0    0    -1  
$EndComp
$Comp
L USB_OTG-RESCUE-ftuart2 J1
U 1 1 5A62D5D0
P 1075 2075
F 0 "J1" H 875 2525 50  0000 L CNN
F 1 "USB_OTG" H 875 2425 50  0000 L CNN
F 2 "Connect:USB_Mini-B" H 1225 2025 50  0001 C CNN
F 3 "" H 1225 2025 50  0001 C CNN
	1    1075 2075
	1    0    0    -1  
$EndComp
$Comp
L L_Core_Ferrite L1
U 1 1 5A62E499
P 1900 1875
F 0 "L1" V 1850 1875 50  0000 C CNN
F 1 "800mA+/FB" V 2000 1875 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1900 1875 50  0001 C CNN
F 3 "" H 1900 1875 50  0001 C CNN
	1    1900 1875
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR01
U 1 1 5A630278
P 1025 2625
F 0 "#PWR01" H 1025 2375 50  0001 C CNN
F 1 "GND" H 1025 2475 50  0000 C CNN
F 2 "" H 1025 2625 50  0001 C CNN
F 3 "" H 1025 2625 50  0001 C CNN
	1    1025 2625
	1    0    0    -1  
$EndComp
$Comp
L USBLC6-2SC6 U1
U 1 1 5A63052D
P 1975 2575
F 0 "U1" H 1725 2925 50  0000 C CNN
F 1 "IP4220" H 1975 2225 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-6_Handsoldering" H 2625 2925 50  0001 C CNN
F 3 "" H 1725 2925 50  0001 C CNN
	1    1975 2575
	1    0    0    -1  
$EndComp
$Comp
L VBUS #PWR02
U 1 1 5A630C7A
P 1475 1825
F 0 "#PWR02" H 1475 1675 50  0001 C CNN
F 1 "VBUS" H 1475 1975 50  0000 C CNN
F 2 "" H 1475 1825 50  0001 C CNN
F 3 "" H 1475 1825 50  0001 C CNN
	1    1475 1825
	1    0    0    -1  
$EndComp
Text Label 2000 2075 0    60   ~ 0
DP
Text Label 2000 2175 0    60   ~ 0
DM
Text Label 4725 2975 0    60   ~ 0
DP
Text Label 4725 2875 0    60   ~ 0
DM
$Comp
L +5V #PWR03
U 1 1 5A632705
P 2475 1825
F 0 "#PWR03" H 2475 1675 50  0001 C CNN
F 1 "+5V" H 2475 1965 50  0000 C CNN
F 2 "" H 2475 1825 50  0001 C CNN
F 3 "" H 2475 1825 50  0001 C CNN
	1    2475 1825
	1    0    0    -1  
$EndComp
$Comp
L VBUS #PWR04
U 1 1 5A632B81
P 2525 2575
F 0 "#PWR04" H 2525 2425 50  0001 C CNN
F 1 "VBUS" V 2575 2650 50  0000 C CNN
F 2 "" H 2525 2575 50  0001 C CNN
F 3 "" H 2525 2575 50  0001 C CNN
	1    2525 2575
	0    1    1    0   
$EndComp
$Comp
L LM1117-3.3-RESCUE-ftuart2 U3
U 1 1 5A633269
P 3450 1875
F 0 "U3" H 3300 2000 50  0000 C CNN
F 1 "LM1117-3.3" H 3450 2000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 3450 1875 50  0001 C CNN
F 3 "" H 3450 1875 50  0001 C CNN
	1    3450 1875
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C4
U 1 1 5A6337F4
P 4000 2000
F 0 "C4" H 4010 2070 50  0000 L CNN
F 1 "10uf" V 4100 1900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4000 2000 50  0001 C CNN
F 3 "" H 4000 2000 50  0001 C CNN
	1    4000 2000
	1    0    0    -1  
$EndComp
$Comp
L C_Small C3
U 1 1 5A6338D4
P 3825 2000
F 0 "C3" H 3835 2070 50  0000 L CNN
F 1 "100nF" V 3725 1900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3825 2000 50  0001 C CNN
F 3 "" H 3825 2000 50  0001 C CNN
	1    3825 2000
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C2
U 1 1 5A633EE3
P 3025 2000
F 0 "C2" H 3035 2070 50  0000 L CNN
F 1 "10uf" V 3125 1900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3025 2000 50  0001 C CNN
F 3 "" H 3025 2000 50  0001 C CNN
	1    3025 2000
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 5A633EE9
P 2850 2000
F 0 "C1" H 2860 2070 50  0000 L CNN
F 1 "100nF" V 2750 1900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2850 2000 50  0001 C CNN
F 3 "" H 2850 2000 50  0001 C CNN
	1    2850 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5A634414
P 3450 2300
F 0 "#PWR05" H 3450 2050 50  0001 C CNN
F 1 "GND" H 3450 2150 50  0000 C CNN
F 2 "" H 3450 2300 50  0001 C CNN
F 3 "" H 3450 2300 50  0001 C CNN
	1    3450 2300
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 5A63483F
P 4225 1825
F 0 "#PWR06" H 4225 1675 50  0001 C CNN
F 1 "+3.3V" H 4225 1965 50  0000 C CNN
F 2 "" H 4225 1825 50  0001 C CNN
F 3 "" H 4225 1825 50  0001 C CNN
	1    4225 1825
	1    0    0    -1  
$EndComp
NoConn ~ 1475 2775
NoConn ~ 2475 2775
$Comp
L +1V8 #PWR07
U 1 1 5A6354BB
P 4600 2050
F 0 "#PWR07" H 4600 1900 50  0001 C CNN
F 1 "+1V8" H 4600 2190 50  0000 C CNN
F 2 "" H 4600 2050 50  0001 C CNN
F 3 "" H 4600 2050 50  0001 C CNN
	1    4600 2050
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C7
U 1 1 5A635742
P 4600 2200
F 0 "C7" H 4610 2270 50  0000 L CNN
F 1 "3.3uF" H 4610 2120 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4600 2200 50  0001 C CNN
F 3 "" H 4600 2200 50  0001 C CNN
	1    4600 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5A635B2C
P 4600 2350
F 0 "#PWR08" H 4600 2100 50  0001 C CNN
F 1 "GND" H 4600 2200 50  0000 C CNN
F 2 "" H 4600 2350 50  0001 C CNN
F 3 "" H 4600 2350 50  0001 C CNN
	1    4600 2350
	1    0    0    -1  
$EndComp
$Comp
L +1V8 #PWR09
U 1 1 5A635F42
P 5950 1325
F 0 "#PWR09" H 5950 1175 50  0001 C CNN
F 1 "+1V8" H 5950 1465 50  0000 C CNN
F 2 "" H 5950 1325 50  0001 C CNN
F 3 "" H 5950 1325 50  0001 C CNN
	1    5950 1325
	1    0    0    -1  
$EndComp
$Comp
L L_Core_Ferrite L2
U 1 1 5A636220
P 4325 750
F 0 "L2" V 4275 750 50  0000 C CNN
F 1 "800mA+/FB" V 4425 750 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4325 750 50  0001 C CNN
F 3 "" H 4325 750 50  0001 C CNN
	1    4325 750 
	0    -1   -1   0   
$EndComp
$Comp
L L_Core_Ferrite L3
U 1 1 5A6362C3
P 4325 900
F 0 "L3" V 4275 900 50  0000 C CNN
F 1 "800mA+/FB" V 4225 925 50  0000 C CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4325 900 50  0001 C CNN
F 3 "" H 4325 900 50  0001 C CNN
	1    4325 900 
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C8
U 1 1 5A6363A1
P 4700 1075
F 0 "C8" H 4710 1145 50  0000 L CNN
F 1 "100nF" V 4600 975 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4700 1075 50  0001 C CNN
F 3 "" H 4700 1075 50  0001 C CNN
	1    4700 1075
	1    0    0    -1  
$EndComp
$Comp
L C_Small C9
U 1 1 5A63644D
P 4900 1075
F 0 "C9" H 4910 1145 50  0000 L CNN
F 1 "100nF" V 4800 975 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4900 1075 50  0001 C CNN
F 3 "" H 4900 1075 50  0001 C CNN
	1    4900 1075
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C10
U 1 1 5A6364BB
P 5125 1075
F 0 "C10" H 5135 1145 50  0000 L CNN
F 1 "4.7uf" V 5225 975 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5125 1075 50  0001 C CNN
F 3 "" H 5125 1075 50  0001 C CNN
	1    5125 1075
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C11
U 1 1 5A63683A
P 5325 1075
F 0 "C11" H 5335 1145 50  0000 L CNN
F 1 "4.7uf" V 5425 975 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5325 1075 50  0001 C CNN
F 3 "" H 5325 1075 50  0001 C CNN
	1    5325 1075
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5A63735D
P 4900 1325
F 0 "#PWR010" H 4900 1075 50  0001 C CNN
F 1 "GND" H 4900 1175 50  0000 C CNN
F 2 "" H 4900 1325 50  0001 C CNN
F 3 "" H 4900 1325 50  0001 C CNN
	1    4900 1325
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR011
U 1 1 5A6377DF
P 3900 725
F 0 "#PWR011" H 3900 575 50  0001 C CNN
F 1 "+3.3V" H 3900 865 50  0000 C CNN
F 2 "" H 3900 725 50  0001 C CNN
F 3 "" H 3900 725 50  0001 C CNN
	1    3900 725 
	1    0    0    -1  
$EndComp
$Comp
L C_Small C12
U 1 1 5A637CF8
P 6275 950
F 0 "C12" H 6285 1020 50  0000 L CNN
F 1 "100nF" V 6175 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6275 950 50  0001 C CNN
F 3 "" H 6275 950 50  0001 C CNN
	1    6275 950 
	1    0    0    -1  
$EndComp
$Comp
L C_Small C13
U 1 1 5A637DD9
P 6500 950
F 0 "C13" H 6510 1020 50  0000 L CNN
F 1 "100nF" V 6400 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6500 950 50  0001 C CNN
F 3 "" H 6500 950 50  0001 C CNN
	1    6500 950 
	1    0    0    -1  
$EndComp
$Comp
L C_Small C14
U 1 1 5A637E99
P 6725 950
F 0 "C14" H 6735 1020 50  0000 L CNN
F 1 "100nF" V 6625 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6725 950 50  0001 C CNN
F 3 "" H 6725 950 50  0001 C CNN
	1    6725 950 
	1    0    0    -1  
$EndComp
$Comp
L C_Small C15
U 1 1 5A637F7D
P 7225 950
F 0 "C15" H 7235 1020 50  0000 L CNN
F 1 "100nF" V 7125 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7225 950 50  0001 C CNN
F 3 "" H 7225 950 50  0001 C CNN
	1    7225 950 
	1    0    0    1   
$EndComp
$Comp
L C_Small C16
U 1 1 5A637F83
P 7450 950
F 0 "C16" H 7460 1020 50  0000 L CNN
F 1 "100nF" V 7350 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7450 950 50  0001 C CNN
F 3 "" H 7450 950 50  0001 C CNN
	1    7450 950 
	1    0    0    -1  
$EndComp
$Comp
L C_Small C17
U 1 1 5A637F89
P 7675 950
F 0 "C17" H 7685 1020 50  0000 L CNN
F 1 "100nF" V 7575 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7675 950 50  0001 C CNN
F 3 "" H 7675 950 50  0001 C CNN
	1    7675 950 
	1    0    0    -1  
$EndComp
$Comp
L C_Small C18
U 1 1 5A6380CB
P 7900 950
F 0 "C18" H 7910 1020 50  0000 L CNN
F 1 "100nF" V 7800 850 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7900 950 50  0001 C CNN
F 3 "" H 7900 950 50  0001 C CNN
	1    7900 950 
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR012
U 1 1 5A63867F
P 6350 1325
F 0 "#PWR012" H 6350 1175 50  0001 C CNN
F 1 "+3.3V" H 6350 1465 50  0000 C CNN
F 2 "" H 6350 1325 50  0001 C CNN
F 3 "" H 6350 1325 50  0001 C CNN
	1    6350 1325
	1    0    0    -1  
$EndComp
$Comp
L +1V8 #PWR013
U 1 1 5A639309
P 6500 725
F 0 "#PWR013" H 6500 575 50  0001 C CNN
F 1 "+1V8" H 6500 865 50  0000 C CNN
F 2 "" H 6500 725 50  0001 C CNN
F 3 "" H 6500 725 50  0001 C CNN
	1    6500 725 
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR014
U 1 1 5A639359
P 7450 725
F 0 "#PWR014" H 7450 575 50  0001 C CNN
F 1 "+3.3V" H 7450 865 50  0000 C CNN
F 2 "" H 7450 725 50  0001 C CNN
F 3 "" H 7450 725 50  0001 C CNN
	1    7450 725 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5A6389F8
P 6725 1150
F 0 "#PWR015" H 6725 900 50  0001 C CNN
F 1 "GND" H 6725 1000 50  0000 C CNN
F 2 "" H 6725 1150 50  0001 C CNN
F 3 "" H 6725 1150 50  0001 C CNN
	1    6725 1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 5A63A7FE
P 7450 1175
F 0 "#PWR016" H 7450 925 50  0001 C CNN
F 1 "GND" H 7450 1025 50  0000 C CNN
F 2 "" H 7450 1175 50  0001 C CNN
F 3 "" H 7450 1175 50  0001 C CNN
	1    7450 1175
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 5A63B031
P 4225 3175
F 0 "R5" V 4305 3175 50  0000 C CNN
F 1 "12K" V 4225 3175 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 4155 3175 50  0001 C CNN
F 3 "" H 4225 3175 50  0001 C CNN
	1    4225 3175
	0    1    1    0   
$EndComp
$Comp
L GND #PWR017
U 1 1 5A63B7C9
P 4000 3175
F 0 "#PWR017" H 4000 2925 50  0001 C CNN
F 1 "GND" H 4000 3025 50  0000 C CNN
F 2 "" H 4000 3175 50  0001 C CNN
F 3 "" H 4000 3175 50  0001 C CNN
	1    4000 3175
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5A63BDC3
P 4250 3375
F 0 "R6" V 4330 3375 50  0000 C CNN
F 1 "10K" V 4250 3375 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 4180 3375 50  0001 C CNN
F 3 "" H 4250 3375 50  0001 C CNN
	1    4250 3375
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR018
U 1 1 5A63BF7C
P 4000 3375
F 0 "#PWR018" H 4000 3225 50  0001 C CNN
F 1 "+3.3V" H 4000 3515 50  0000 C CNN
F 2 "" H 4000 3375 50  0001 C CNN
F 3 "" H 4000 3375 50  0001 C CNN
	1    4000 3375
	0    -1   -1   0   
$EndComp
$Comp
L 93CxxC U2
U 1 1 5A63CCAD
P 3250 5000
F 0 "U2" H 3100 5350 50  0000 C CNN
F 1 "93CxxC" H 3050 4750 50  0000 L CNN
F 2 "Housings_SSOP:TSSOP-8_4.4x3mm_Pitch0.65mm" H 3150 5000 50  0001 C CNN
F 3 "" H 3250 5100 50  0001 C CNN
	1    3250 5000
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5A63F8DB
P 4075 4900
F 0 "R4" V 4155 4900 50  0000 C CNN
F 1 "2.2K" V 4075 4900 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 4005 4900 50  0001 C CNN
F 3 "" H 4075 4900 50  0001 C CNN
	1    4075 4900
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 5A640140
P 3575 4175
F 0 "R1" V 3655 4175 50  0000 C CNN
F 1 "10K" V 3575 4175 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 3505 4175 50  0001 C CNN
F 3 "" H 3575 4175 50  0001 C CNN
	1    3575 4175
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 5A6402F0
P 3775 4175
F 0 "R2" V 3855 4175 50  0000 C CNN
F 1 "10K" V 3775 4175 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 3705 4175 50  0001 C CNN
F 3 "" H 3775 4175 50  0001 C CNN
	1    3775 4175
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 5A640396
P 3950 4175
F 0 "R3" V 4030 4175 50  0000 C CNN
F 1 "10K" V 3950 4175 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 3880 4175 50  0001 C CNN
F 3 "" H 3950 4175 50  0001 C CNN
	1    3950 4175
	-1   0    0    1   
$EndComp
$Comp
L +3.3V #PWR019
U 1 1 5A640924
P 3775 3900
F 0 "#PWR019" H 3775 3750 50  0001 C CNN
F 1 "+3.3V" H 3775 4040 50  0000 C CNN
F 2 "" H 3775 3900 50  0001 C CNN
F 3 "" H 3775 3900 50  0001 C CNN
	1    3775 3900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 5A65DC22
P 2775 5100
F 0 "#PWR020" H 2775 4850 50  0001 C CNN
F 1 "GND" H 2775 4950 50  0000 C CNN
F 2 "" H 2775 5100 50  0001 C CNN
F 3 "" H 2775 5100 50  0001 C CNN
	1    2775 5100
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR021
U 1 1 5A65DFC5
P 3650 5100
F 0 "#PWR021" H 3650 4950 50  0001 C CNN
F 1 "+3.3V" H 3650 5240 50  0000 C CNN
F 2 "" H 3650 5100 50  0001 C CNN
F 3 "" H 3650 5100 50  0001 C CNN
	1    3650 5100
	0    1    1    0   
$EndComp
NoConn ~ 3550 5000
$Comp
L Crystal Y1
U 1 1 5A65EBE4
P 4250 5375
F 0 "Y1" H 4250 5525 50  0000 C CNN
F 1 "12MHz" H 4250 5225 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_5032_2Pads" H 4250 5375 50  0001 C CNN
F 3 "" H 4250 5375 50  0001 C CNN
	1    4250 5375
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 5A65F21C
P 4025 5600
F 0 "C5" H 4050 5700 50  0000 L CNN
F 1 "27p" H 4050 5500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4063 5450 50  0001 C CNN
F 3 "" H 4025 5600 50  0001 C CNN
	1    4025 5600
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 5A65F384
P 4450 5600
F 0 "C6" H 4475 5700 50  0000 L CNN
F 1 "27p" H 4475 5500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4488 5450 50  0001 C CNN
F 3 "" H 4450 5600 50  0001 C CNN
	1    4450 5600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 5A65F800
P 4250 5850
F 0 "#PWR022" H 4250 5600 50  0001 C CNN
F 1 "GND" H 4250 5700 50  0000 C CNN
F 2 "" H 4250 5850 50  0001 C CNN
F 3 "" H 4250 5850 50  0001 C CNN
	1    4250 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 5A660918
P 5950 6200
F 0 "#PWR023" H 5950 5950 50  0001 C CNN
F 1 "GND" H 5950 6050 50  0000 C CNN
F 2 "" H 5950 6200 50  0001 C CNN
F 3 "" H 5950 6200 50  0001 C CNN
	1    5950 6200
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-ftuart2 D1
U 1 1 5A661E41
P 8300 1300
F 0 "D1" H 8300 1400 50  0000 C CNN
F 1 "RX" H 8300 1200 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8300 1300 50  0001 C CNN
F 3 "" H 8300 1300 50  0001 C CNN
	1    8300 1300
	0    -1   -1   0   
$EndComp
$Comp
L R R7
U 1 1 5A66209C
P 8300 925
F 0 "R7" V 8380 925 50  0000 C CNN
F 1 "1K" V 8300 925 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 8230 925 50  0001 C CNN
F 3 "" H 8300 925 50  0001 C CNN
	1    8300 925 
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-ftuart2 D2
U 1 1 5A6625E6
P 8575 1300
F 0 "D2" H 8575 1400 50  0000 C CNN
F 1 "TX" H 8575 1200 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8575 1300 50  0001 C CNN
F 3 "" H 8575 1300 50  0001 C CNN
	1    8575 1300
	0    -1   -1   0   
$EndComp
$Comp
L R R8
U 1 1 5A6625EC
P 8575 925
F 0 "R8" V 8655 925 50  0000 C CNN
F 1 "1K" V 8575 925 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 8505 925 50  0001 C CNN
F 3 "" H 8575 925 50  0001 C CNN
	1    8575 925 
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-ftuart2 D3
U 1 1 5A662781
P 8875 1300
F 0 "D3" H 8875 1400 50  0000 C CNN
F 1 "RX" H 8875 1200 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8875 1300 50  0001 C CNN
F 3 "" H 8875 1300 50  0001 C CNN
	1    8875 1300
	0    -1   -1   0   
$EndComp
$Comp
L R R9
U 1 1 5A662787
P 8875 925
F 0 "R9" V 8955 925 50  0000 C CNN
F 1 "1K" V 8875 925 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 8805 925 50  0001 C CNN
F 3 "" H 8875 925 50  0001 C CNN
	1    8875 925 
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-ftuart2 D4
U 1 1 5A66278E
P 9150 1300
F 0 "D4" H 9150 1400 50  0000 C CNN
F 1 "TX" H 9150 1200 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 9150 1300 50  0001 C CNN
F 3 "" H 9150 1300 50  0001 C CNN
	1    9150 1300
	0    -1   -1   0   
$EndComp
$Comp
L R R10
U 1 1 5A662794
P 9150 925
F 0 "R10" V 9230 925 50  0000 C CNN
F 1 "1K" V 9150 925 50  0000 C CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" V 9080 925 50  0001 C CNN
F 3 "" H 9150 925 50  0001 C CNN
	1    9150 925 
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR024
U 1 1 5A66381F
P 8575 700
F 0 "#PWR024" H 8575 550 50  0001 C CNN
F 1 "+3.3V" H 8575 840 50  0000 C CNN
F 2 "" H 8575 700 50  0001 C CNN
F 3 "" H 8575 700 50  0001 C CNN
	1    8575 700 
	1    0    0    -1  
$EndComp
Text Label 7300 3075 0    60   ~ 0
RXLED1
Text Label 7300 3175 0    60   ~ 0
TXLED1
Text Label 7325 4875 0    60   ~ 0
RXLED2
Text Label 7325 4975 0    60   ~ 0
TXLED2
Text Label 8300 1500 3    60   ~ 0
RXLED1
Text Label 8575 1500 3    60   ~ 0
TXLED1
Text Label 8875 1525 3    60   ~ 0
RXLED2
Text Label 9150 1525 3    60   ~ 0
TXLED2
Text Label 7300 3775 0    60   ~ 0
RX2
Text Label 7300 3675 0    60   ~ 0
TX2
Text Label 7300 3875 0    60   ~ 0
RTS2
Text Label 7300 3975 0    60   ~ 0
CTS2
Text Label 7450 1975 2    60   ~ 0
RX1
Text Label 7450 1875 2    60   ~ 0
TX1
Text Label 7450 2075 2    60   ~ 0
RTS1
Text Label 7300 2175 0    60   ~ 0
CTS1
NoConn ~ 7250 2275
NoConn ~ 7250 2375
NoConn ~ 7250 2475
NoConn ~ 7250 2575
NoConn ~ 7250 2775
NoConn ~ 7250 2875
NoConn ~ 7250 2975
NoConn ~ 7250 3275
NoConn ~ 7250 3375
NoConn ~ 7250 3475
NoConn ~ 7250 4575
NoConn ~ 7250 4675
NoConn ~ 7250 4775
NoConn ~ 7250 5075
NoConn ~ 7250 5175
NoConn ~ 7250 5275
NoConn ~ 7250 5475
NoConn ~ 7250 5575
$Comp
L Conn_01x05 J2
U 1 1 5AABC6EE
P 8675 2550
F 0 "J2" H 8675 2850 50  0000 C CNN
F 1 "UART1" H 8675 2250 50  0000 C CNN
F 2 "ftuart2:Pin_Header_Angled_1x05_smd" H 8675 2550 50  0001 C CNN
F 3 "" H 8675 2550 50  0001 C CNN
	1    8675 2550
	1    0    0    -1  
$EndComp
Text Label 8300 2550 0    60   ~ 0
TX1
Text Label 8300 2650 0    60   ~ 0
RX1
Text Label 8300 2450 0    60   ~ 0
RTS1
Text Label 8300 2350 0    60   ~ 0
CTS1
$Comp
L GND #PWR025
U 1 1 5AABD6E9
P 8300 2750
F 0 "#PWR025" H 8300 2500 50  0001 C CNN
F 1 "GND" H 8300 2600 50  0000 C CNN
F 2 "" H 8300 2750 50  0001 C CNN
F 3 "" H 8300 2750 50  0001 C CNN
	1    8300 2750
	0    1    1    0   
$EndComp
$Comp
L Conn_01x05 J3
U 1 1 5AABDE1D
P 8675 3300
F 0 "J3" H 8675 3600 50  0000 C CNN
F 1 "UART2" H 8675 3000 50  0000 C CNN
F 2 "ftuart2:Pin_Header_Angled_1x05_smd" H 8675 3300 50  0001 C CNN
F 3 "" H 8675 3300 50  0001 C CNN
	1    8675 3300
	1    0    0    1   
$EndComp
Text Label 8300 3300 0    60   ~ 0
TX2
Text Label 8300 3400 0    60   ~ 0
RX2
Text Label 8300 3200 0    60   ~ 0
RTS2
Text Label 8300 3100 0    60   ~ 0
CTS2
$Comp
L GND #PWR026
U 1 1 5AABDE2C
P 8300 3500
F 0 "#PWR026" H 8300 3250 50  0001 C CNN
F 1 "GND" H 8300 3350 50  0000 C CNN
F 2 "" H 8300 3500 50  0001 C CNN
F 3 "" H 8300 3500 50  0001 C CNN
	1    8300 3500
	0    1    1    0   
$EndComp
Connection ~ 7675 775 
Wire Wire Line
	7900 775  7900 850 
Connection ~ 7450 775 
Wire Wire Line
	7675 775  7675 850 
Wire Wire Line
	7450 725  7450 850 
Wire Wire Line
	7225 775  7900 775 
Wire Wire Line
	7225 850  7225 775 
Connection ~ 6500 775 
Wire Wire Line
	6725 775  6725 850 
Wire Wire Line
	6275 775  6725 775 
Wire Wire Line
	6275 850  6275 775 
Connection ~ 7675 1125
Wire Wire Line
	7900 1125 7900 1050
Connection ~ 7450 1125
Wire Wire Line
	7675 1125 7675 1050
Wire Wire Line
	7450 1050 7450 1175
Wire Wire Line
	7225 1125 7225 1050
Connection ~ 6725 1125
Connection ~ 6450 1375
Connection ~ 6350 1375
Wire Wire Line
	6250 1375 6550 1375
Connection ~ 6500 1125
Wire Wire Line
	6500 1050 6500 1125
Wire Wire Line
	6725 1050 6725 1150
Wire Wire Line
	6275 1050 6275 1125
Connection ~ 3900 750 
Wire Wire Line
	3900 900  4175 900 
Wire Wire Line
	3900 725  3900 900 
Wire Wire Line
	4175 750  3900 750 
Connection ~ 4900 1250
Wire Wire Line
	4900 1175 4900 1325
Connection ~ 5125 1250
Wire Wire Line
	5125 1250 5125 1175
Wire Wire Line
	5325 1250 5325 1175
Wire Wire Line
	4700 1250 5325 1250
Wire Wire Line
	4700 1175 4700 1250
Connection ~ 5325 900 
Wire Wire Line
	5325 975  5325 900 
Connection ~ 5125 750 
Wire Wire Line
	5125 975  5125 750 
Connection ~ 4900 900 
Wire Wire Line
	4900 975  4900 900 
Connection ~ 4700 750 
Wire Wire Line
	4700 975  4700 750 
Wire Wire Line
	5550 900  4475 900 
Wire Wire Line
	4475 750  5650 750 
Connection ~ 5950 1375
Wire Wire Line
	5850 1375 6050 1375
Wire Wire Line
	4600 2350 4600 2300
Connection ~ 4600 2075
Wire Wire Line
	4600 2075 4850 2075
Wire Wire Line
	4600 2050 4600 2100
Connection ~ 4225 1875
Wire Wire Line
	4225 1875 4225 1825
Connection ~ 3825 2200
Wire Wire Line
	4000 2200 4000 2100
Connection ~ 3450 2200
Wire Wire Line
	3825 2200 3825 2100
Connection ~ 3025 2200
Wire Wire Line
	3025 2100 3025 2200
Wire Wire Line
	3450 2175 3450 2300
Wire Wire Line
	2850 2200 4000 2200
Wire Wire Line
	2850 2100 2850 2200
Connection ~ 4000 1875
Wire Wire Line
	4000 1900 4000 1875
Connection ~ 3825 1875
Wire Wire Line
	3825 1900 3825 1875
Connection ~ 3025 1875
Wire Wire Line
	3025 1900 3025 1875
Connection ~ 2850 1875
Wire Wire Line
	2850 1900 2850 1875
Wire Wire Line
	2475 2575 2525 2575
Wire Wire Line
	2050 1875 3150 1875
Wire Wire Line
	2475 1875 2475 1825
Connection ~ 1475 1875
Wire Wire Line
	1475 1875 1475 1825
Wire Wire Line
	7250 3975 7450 3975
Wire Wire Line
	7250 3875 7450 3875
Wire Wire Line
	7250 3775 7450 3775
Wire Wire Line
	7250 3675 7450 3675
Wire Wire Line
	6550 1375 6550 1575
Wire Wire Line
	6450 1375 6450 1575
Wire Wire Line
	6350 1325 6350 1575
Wire Wire Line
	6250 1575 6250 1375
Wire Wire Line
	6050 1375 6050 1575
Wire Wire Line
	5950 1325 5950 1575
Wire Wire Line
	5850 1375 5850 1575
Wire Wire Line
	3750 1875 4850 1875
Wire Wire Line
	4400 5375 4850 5375
Wire Wire Line
	4450 4975 4850 4975
Wire Wire Line
	2775 4575 4850 4575
Wire Wire Line
	2700 4475 4850 4475
Wire Wire Line
	4400 3375 4850 3375
Wire Wire Line
	4850 2975 4650 2975
Wire Wire Line
	4850 2875 4650 2875
Connection ~ 2475 1875
Connection ~ 2475 2175
Wire Wire Line
	2475 2175 2475 2375
Connection ~ 1475 2075
Wire Wire Line
	1475 2375 1475 2075
Connection ~ 1075 2575
Wire Wire Line
	1375 2175 2550 2175
Wire Wire Line
	1375 2075 2550 2075
Connection ~ 1025 2575
Wire Wire Line
	1025 2575 1025 2625
Wire Wire Line
	1075 2575 1075 2475
Wire Wire Line
	975  2575 1475 2575
Wire Wire Line
	975  2475 975  2575
Wire Wire Line
	1375 1875 1750 1875
Wire Wire Line
	6275 1125 6725 1125
Wire Wire Line
	7225 1125 7900 1125
Wire Wire Line
	4000 3175 4075 3175
Wire Wire Line
	4000 3375 4100 3375
Wire Wire Line
	4375 3175 4850 3175
Wire Wire Line
	2700 4475 2700 4900
Wire Wire Line
	2700 4900 2950 4900
Wire Wire Line
	2775 4575 2775 5000
Wire Wire Line
	2775 5000 2950 5000
Wire Wire Line
	3675 4675 4850 4675
Wire Wire Line
	3550 4900 3925 4900
Connection ~ 4350 4675
Wire Wire Line
	4350 4675 4350 4900
Wire Wire Line
	4350 4900 4225 4900
Wire Wire Line
	3675 4675 3675 4800
Wire Wire Line
	3675 4800 3550 4800
Wire Wire Line
	3575 4025 3575 3950
Wire Wire Line
	3575 3950 3950 3950
Wire Wire Line
	3775 3900 3775 4025
Wire Wire Line
	3950 3950 3950 4025
Connection ~ 3775 3950
Wire Wire Line
	3575 4325 3575 4475
Connection ~ 3575 4475
Wire Wire Line
	3775 4325 3775 4900
Connection ~ 3775 4900
Wire Wire Line
	3950 4325 3950 4575
Connection ~ 3950 4575
Wire Wire Line
	2950 4800 2850 4800
Wire Wire Line
	2850 4800 2850 5300
Wire Wire Line
	2850 5300 3600 5300
Wire Wire Line
	3600 5300 3600 5100
Wire Wire Line
	3550 5100 3650 5100
Wire Wire Line
	2950 5100 2775 5100
Connection ~ 3600 5100
Connection ~ 4450 5375
Wire Wire Line
	4450 4975 4450 5125
Wire Wire Line
	4450 5125 4025 5125
Wire Wire Line
	4025 5125 4025 5450
Wire Wire Line
	4025 5375 4100 5375
Connection ~ 4025 5375
Wire Wire Line
	4450 5375 4450 5450
Wire Wire Line
	4450 5750 4450 5800
Wire Wire Line
	4025 5800 4750 5800
Wire Wire Line
	4025 5800 4025 5750
Wire Wire Line
	4250 5800 4250 5850
Connection ~ 4250 5800
Wire Wire Line
	4850 5575 4750 5575
Wire Wire Line
	4750 5575 4750 5800
Connection ~ 4450 5800
Wire Wire Line
	5450 5975 5450 6150
Wire Wire Line
	5450 6150 6350 6150
Wire Wire Line
	5650 6150 5650 5975
Wire Wire Line
	5750 6150 5750 5975
Connection ~ 5650 6150
Wire Wire Line
	5850 6150 5850 5975
Connection ~ 5750 6150
Wire Wire Line
	5950 5975 5950 6200
Connection ~ 5850 6150
Wire Wire Line
	6050 6150 6050 5975
Connection ~ 5950 6150
Wire Wire Line
	6150 6150 6150 5975
Connection ~ 6050 6150
Wire Wire Line
	6250 6150 6250 5975
Connection ~ 6150 6150
Wire Wire Line
	6350 6150 6350 5975
Connection ~ 6250 6150
Wire Wire Line
	7250 3075 7450 3075
Wire Wire Line
	7250 3175 7450 3175
Wire Wire Line
	7250 4875 7475 4875
Wire Wire Line
	7250 4975 7475 4975
Wire Wire Line
	8300 1075 8300 1150
Wire Wire Line
	8575 1075 8575 1150
Wire Wire Line
	8875 1075 8875 1150
Wire Wire Line
	9150 1075 9150 1150
Wire Wire Line
	8300 775  8300 750 
Wire Wire Line
	8300 750  9150 750 
Wire Wire Line
	8575 700  8575 775 
Wire Wire Line
	8875 750  8875 775 
Connection ~ 8575 750 
Wire Wire Line
	9150 750  9150 775 
Connection ~ 8875 750 
Wire Wire Line
	8300 1450 8300 1625
Wire Wire Line
	8575 1450 8575 1600
Wire Wire Line
	8875 1450 8875 1600
Wire Wire Line
	9150 1450 9150 1625
Wire Wire Line
	7250 2175 7450 2175
Wire Wire Line
	7250 2075 7450 2075
Wire Wire Line
	7250 1975 7450 1975
Wire Wire Line
	7250 1875 7450 1875
Wire Wire Line
	8475 2350 8300 2350
Wire Wire Line
	8475 2450 8300 2450
Wire Wire Line
	8475 2550 8300 2550
Wire Wire Line
	8475 2650 8300 2650
Wire Wire Line
	8475 2750 8300 2750
Wire Wire Line
	8475 3100 8300 3100
Wire Wire Line
	8475 3200 8300 3200
Wire Wire Line
	8475 3300 8300 3300
Wire Wire Line
	8475 3400 8300 3400
Wire Wire Line
	8475 3500 8300 3500
Wire Wire Line
	7250 4075 7450 4075
Text Label 7450 4075 2    60   ~ 0
D4
Text Label 7445 4175 2    60   ~ 0
D5
Text Label 7445 4275 2    60   ~ 0
D6
Text Label 7445 4370 2    60   ~ 0
D7
Wire Wire Line
	7250 4175 7445 4175
Wire Wire Line
	7250 4275 7445 4275
Wire Wire Line
	7250 4375 7445 4375
Wire Wire Line
	7445 4375 7445 4370
$Comp
L Conn_01x05 J4
U 1 1 5AC3ADC2
P 8675 4210
F 0 "J4" H 8675 4510 50  0000 C CNN
F 1 "DATA" H 8675 3910 50  0000 C CNN
F 2 "ftuart2:Pin_Header_Angled_1x05_smd" H 8675 4210 50  0001 C CNN
F 3 "" H 8675 4210 50  0001 C CNN
	1    8675 4210
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 5AC3ADCC
P 8300 4410
F 0 "#PWR027" H 8300 4160 50  0001 C CNN
F 1 "GND" H 8300 4260 50  0000 C CNN
F 2 "" H 8300 4410 50  0001 C CNN
F 3 "" H 8300 4410 50  0001 C CNN
	1    8300 4410
	0    1    1    0   
$EndComp
Wire Wire Line
	8475 4010 8300 4010
Wire Wire Line
	8475 4110 8300 4110
Wire Wire Line
	8475 4210 8300 4210
Wire Wire Line
	8475 4310 8300 4310
Wire Wire Line
	8475 4410 8300 4410
Text Label 8300 4010 0    60   ~ 0
D4
Text Label 8300 4110 0    60   ~ 0
D5
Text Label 8300 4210 0    60   ~ 0
D6
Text Label 8300 4310 0    60   ~ 0
D7
NoConn ~ 1380 2275
Wire Wire Line
	6500 725  6500 850 
Wire Wire Line
	5650 750  5650 1575
Wire Wire Line
	5550 1575 5550 900 
$EndSCHEMATC
